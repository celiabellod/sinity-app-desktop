import { Home, Users } from 'react-feather'

export default [
  {
    id: 'home',
    title: 'Home',
    icon: <Home size={20} />,
    navLink: '/home'
  },
  {
    id: 'listing',
    title: 'Liste des utilisateurs',
    icon: <Users size={20} />,
    navLink: '/apps/user/list'
  }
]
