import axios from "axios";
import { useSelector } from "react-redux";

// ** Get all Data
export const GetLengthData = (authStore) => {  
  return async (dispatch) => {
    //const authStore = useSelector(state => state.auth)
    await axios
      .get(process.env.REACT_APP_API_ENDPOINT + "users/", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((response) => {
        dispatch({
          type: "GET_LENGTH_DATA",
          data: response.data.length,
        });
      });
  };
};

// ** Get data on page or row change
export const GetData = (params, authStore) => {
  return async (dispatch) => {
    await axios
      .get(process.env.REACT_APP_API_ENDPOINT + "users/", {
        params: params,
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((response) => {
        dispatch({
          type: "GET_DATA",
          data: response.data,
          totalPages: response.data.length,
          params,
        });
      });
  };
};

// ** Get User
export const GetUser = (id) => {
  return async (dispatch) => {
    await axios
      .get("/api/users/user", { id })
      .then((response) => {
        dispatch({
          type: "GET_USER",
          selectedUser: response.data.user,
        });
      })
      .catch((err) => console.log(err));
  };
};

// ** Add new user
export const addUser = (user) => {
  return (dispatch, getState) => {
    axios
      .post("/apps/users/add-user", user)
      .then((response) => {
        dispatch({
          type: "ADD_USER",
          user,
        });
      })
      .then(() => {
        // dispatch(GetData(getState().users.params))
        dispatch(GetLengthData());
      })
      .catch((err) => console.log(err));
  };
};

// ** Delete user
export const DeleteUser = (id) => {
  return (dispatch, getState) => {
    axios
      .delete("/apps/users/delete", { id })
      .then((response) => {
        dispatch({
          type: "DELETE_USER",
        });
      })
      .then(() => {
        // dispatch(getData(getState().users.params))
        dispatch(GetLengthData());
      });
  };
};
