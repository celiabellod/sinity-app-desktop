// ** React Imports
import { Link } from 'react-router-dom'

// ** Custom Components
import Avatar from '@components/avatar'

// ** Store & Actions
import { GetUser, DeleteUser } from '../store/action'
import { store } from '@store/storeConfig/store'

// ** Third Party Components
import { Badge, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import { Slack, User, Settings, Database, Edit2, MoreVertical, FileText, Trash2, Archive } from 'react-feather'
import userMessagesFr from '@src/assets/data/locales/fr.json'

// ** Renders Client Columns
const renderClient = row => {
  const stateNum = Math.floor(Math.random() * 6),
    states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
    color = states[stateNum]

  // if (row.avatar.length) {
  //   return <Avatar className='mr-1' img={row.avatar} width='32' height='32' />
  // } else {
  //   return <Avatar color={color || 'primary'} className='mr-1' content={row.fullName || 'John Doe'} initials />
  // }
}

// ** Renders Role Columns
const renderRole = row => {
  const roleObj = {
    subscriber: {
      class: 'text-primary',
      icon: User
    },
    maintainer: {
      class: 'text-success',
      icon: Database
    },
    editor: {
      class: 'text-info',
      icon: Edit2
    },
    author: {
      class: 'text-warning',
      icon: Settings
    },
    admin: {
      class: 'text-danger',
      icon: Slack
    }
  }


  const Icon = roleObj[row.role.name] ? roleObj[row.role.name].icon : Edit2

  return (
    <span className='text-truncate text-capitalize align-middle'>
      <Icon size={18} className={`${roleObj[row.role.name] ? roleObj[row.role.name].class : ''} mr-50`} />
      {row.role.name}
    </span>
  )
}

const renderDate = date => {
  const $date = new Date(date);
  return $date.toLocaleDateString("fr-FR");
}

const statusObj = {
  pending: 'light-warning',
  active: 'light-success',
  inactive: 'light-secondary'
}

export const columns = [
  {
    name: (userMessagesFr['User'] != undefined) ? userMessagesFr['User'] : 'User',
    minWidth: '297px',
    selector: 'firstname',
    sortable: true,
    cell: row => (
      <div className='d-flex justify-content-left align-items-center'>
        {renderClient(row)}
        <div className='d-flex flex-column'>
          <Link
            to={`/apps/user/view/${row.id}`}
            className='user-name text-truncate mb-0'
            onClick={() => store.dispatch(GetUser(row.id))}
          >
            <span className='font-weight-bold'>{row.firstname}</span>
          </Link>
          <small className='text-truncate text-muted mb-0'>{(userMessagesFr[row.gender.name] != undefined) ? userMessagesFr[row.gender.name] : row.gender.name}</small>
        </div>
      </div>
    )
  },
  {
    name: (userMessagesFr['Email'] != undefined) ? userMessagesFr['Email'] : 'Email',
    minWidth: '320px',
    selector: 'email',
    sortable: true,
    cell: row => row.email
  },
  {
    name: (userMessagesFr['Astro sign'] != undefined) ? userMessagesFr['Astro sign'] : 'Astro sign',
    minWidth: '172px',
    selector: 'astro_sign',
    sortable: true,
    cell: row => (userMessagesFr[row.astro_sign.name] != undefined) ? userMessagesFr[row.astro_sign.name] : row.astro_sign.name
  },
  {
    name: (userMessagesFr['Birthdate'] != undefined) ? userMessagesFr['Birthdate'] : 'Birthdate',
    minWidth: '138px',
    selector: 'currentPlan',
    sortable: true,
    cell: row => <span className='text-capitalize'>{renderDate(row.birthdate)}</span>
  },
  {
    name: (userMessagesFr['City'] != undefined) ? userMessagesFr['City'] : 'City',
    minWidth: '138px',
    selector: 'city',
    sortable: true,
    cell: row => row.city.name
  },
  {
    name: 'Actions',
    minWidth: '100px',
    cell: row => (
      <UncontrolledDropdown>
        <DropdownToggle tag='div' className='btn btn-sm'>
          <MoreVertical size={14} className='cursor-pointer' />
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem
            tag={Link}
            to={`/apps/user/view/${row.id}`}
            className='w-100'
            onClick={() => store.dispatch(GetUser(row.id))}
          >
            <FileText size={14} className='mr-50' />
            <span className='align-middle'>{(userMessagesFr['Details'] != undefined) ? userMessagesFr['Details'] : 'Details'}</span>
          </DropdownItem>
          <DropdownItem
            tag={Link}
            to={`/apps/user/edit/${row.id}`}
            className='w-100'
            onClick={() => store.dispatch(GetUser(row.id))}
          >
            <Archive size={14} className='mr-50' />
            <span className='align-middle'>{(userMessagesFr['Edit'] != undefined) ? userMessagesFr['Edit'] : 'Edit'}</span>
          </DropdownItem>
          <DropdownItem className='w-100' onClick={() => store.dispatch(DeleteUser(row.id))}>
            <Trash2 size={14} className='mr-50' />
            <span className='align-middle'>{(userMessagesFr['Delete'] != undefined) ? userMessagesFr['Delete'] : 'Delete'}</span>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    )
  }
]
