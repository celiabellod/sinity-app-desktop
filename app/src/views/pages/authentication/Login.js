import { useState, useContext, Fragment } from "react";
import Avatar from "@components/avatar";
import classnames from 'classnames'
import { useSkin } from "@hooks/useSkin";
import useJwt from "@src/auth/jwt/useJwt";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { toast, Slide } from "react-toastify";
import { HandleLogin } from "@store/actions/auth";
// import { AbilityContext } from '@src/utility/context/Can'
import { Link, useHistory } from "react-router-dom";
import InputPasswordToggle from "@components/input-password-toggle";
import { getHomeRouteForLoggedInUser, isObjEmpty } from "@utils";
import userMessagesFr from '@src/assets/data/locales/fr.json'

import {
  Facebook,
  Twitter,
  Mail,
  GitHub,
  HelpCircle,
  Coffee,
} from "react-feather";
import {
  Card, 
  CardBody,  
  CardTitle,
  CardText,
  Form,
  Input,
  FormGroup,
  Label,
  CustomInput,
  Button,
  UncontrolledTooltip,
} from "reactstrap";

import "@styles/base/pages/page-auth.scss";

const ToastContent = ({ name, role }) => (
  <Fragment>
    <div className="toastify-header">
      <div className="title-wrapper">
        <Avatar size="sm" color="success" icon={<Coffee size={12} />} />
        <h6 className="toast-title font-weight-bold">Bonjour, {name}</h6>
      </div>
    </div>
    <div className="toastify-body">
      <span>
        Vous venez de vous connecter avec succès!
      </span>
    </div>
  </Fragment>
);

const Login = (props) => {
  const [skin, setSkin] = useSkin();
  // const ability = useContext(AbilityContext)
  const dispatch = useDispatch();
  const history = useHistory();
  const [grantType, setGrandType] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [scope, setScope] = useState("");
  const [clientId, setClientId] = useState("");
  const [clientSecret, setClientSecret] = useState("");

  const { jwt } = useJwt({loginEndpoint: process.env.REACT_APP_API_ENDPOINT + 'token/'});

  const { register, errors, handleSubmit } = useForm();
  const [error, setError] = useState()
  // const illustration = skin === "dark" ? "login-v2-dark.svg" : "login-v2.svg";
  // const source = require(`@src/assets/images/pages/${illustration}`).default;

  const onSubmit = (data) => {
    if (isObjEmpty(errors)) {
      jwt
        .login(grantType, email, password, scope, clientId, clientSecret)
        .then((res) => {
          const data = {
            ...res.data.user_data,
            accessToken: res.data.access_token,
            refreshToken: res.data.refreshToken,
          };

          dispatch(HandleLogin(data));
          // ability.update(res.data.userData.ability)
          history.push(getHomeRouteForLoggedInUser("admin"));
          toast.success(
            <ToastContent
              name={data.firstname || "John Doe"}
              role={data.role.name || "admin"}
            />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          );
        })
        .catch((err) => setError(err.response.data.detail))
    }
  };

  return (
    <div className="auth-wrapper auth-v1 px-2">
      <div className="auth-inner py-2">
        <Card className="mb-0">
          <CardBody>
            <Link
              className="brand-logo"
              to="/"
              onClick={(e) => e.preventDefault()}
            >
              <img className='' src={require(`@src/assets/images/logo/logo.png`).default} height='25' />
              <h2 className="brand-text text-primary ml-1">Sinity</h2>
            </Link>
            { error &&
                <p class="text-danger"> { ( userMessagesFr[error] != undefined) ? userMessagesFr[error] : error } </p> }
            <Form
              className="auth-login-form mt-2"
              onSubmit={handleSubmit(onSubmit)}
            >
              <FormGroup>
                <Label className="form-label" for="login-email">
                  Email
                </Label>
                <Input
                  autoFocus
                  type='email'
                  value={email}
                  id='login-email'
                  name='login-email'
                  placeholder='john@example.com'
                  onChange={e => setEmail(e.target.value)}
                  className={classnames({ 'is-invalid': error })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
              </FormGroup>
              <FormGroup>
                <div className="d-flex justify-content-between">
                  <Label className="form-label" for="login-password">
                    Mot de passe
                  </Label>
                </div>
                <InputPasswordToggle
                  value={password}
                  id='login-password'
                  name='login-password'
                  className='input-group-merge'
                  onChange={e => setPassword(e.target.value)}
                  className={classnames({ 'is-invalid': error })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
                 <Link to="/pages/forgot-password">
                    <small >Mot de passe oublié?</small>
                  </Link>
              </FormGroup>
              <FormGroup>
                <CustomInput
                  type="checkbox"
                  id="remember-me"
                  label="Se souvenir de moi"
                />
              </FormGroup>
              <Button.Ripple color="primary" block type='submit'>
                Se connecter
              </Button.Ripple>
            </Form>
            <div className="divider my-2">
              <div className="divider-text">or</div>
            </div>
            <div className="auth-footer-btn d-flex justify-content-center">
              <Button.Ripple color="facebook">
                <Facebook size={14} />
              </Button.Ripple>
              <Button.Ripple color="twitter">
                <Twitter size={14} />
              </Button.Ripple>
              <Button.Ripple color="google">
                <Mail size={14} />
              </Button.Ripple>
              <Button.Ripple className="mr-0" color="github">
                <GitHub size={14} />
              </Button.Ripple>
            </div>
          </CardBody>
        </Card>
      </div>
    </div>
  );
};

export default Login;
